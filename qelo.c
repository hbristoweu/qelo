/* Quick Elo Calculation */

/* TODO
 * x	Write function to compare argv[3] with string literals
 * 		WIN, LOSS/LOSE, DRAW, and interpret as their macro values
 * 		Function returns -1 and program exits on bad input string.
 * 
 *  	Write a catchall bespoke exit function
 * 
 *  	Replace atoi with a safer alternative
 * 
 *  	parse_result() - Use some kind of enum instead of 
 *  	string literals (Union?)
 * */

/* Build: -lm */

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#define WIN		1
#define LOSE	0
#define DRAW	0.5

#define DEFAULT_KF	32



int8_t	g_kf = DEFAULT_KF;



void calc_set_elo(uint32_t comp_1, uint32_t comp_2, float res){

	/* tr   Transformed rating 									*/
	uint64_t tr_1 = pow(10, comp_1 / 400);
	uint64_t tr_2 = pow(10, comp_2 / 400);
	
	/* es   Expected score 										*/
	float es_1 = (double)tr_1 / (tr_1 + tr_2);
	float es_2 = (double)tr_2 / (tr_1 + tr_2);
	
	/* s    Score modifier (Actual result of match) 			*/
	int8_t s_1, s_2;
	
	/* Find and set values for each score modifier 				*/
	s_1 = res;
	if (res == DRAW)
			s_2 = DRAW;
	else s_2 = s_1 ^ 1;     /* s_2 ~= s_1 */
	
	/* ar   Adjusted rating */
	uint32_t ar_1 = comp_1 + g_kf * (s_1 - es_1);
	uint32_t ar_2 = comp_2 + g_kf * (s_2 - es_2);
	
	/* Set new elo ratings for competitors 						*/
	comp_1 = ar_1;
	comp_2 = ar_2;
	
	printf("%.1f\t%d\t%d\n",res,comp_1,comp_2);
}



float parse_result(char *s_result){
	
	if (strcmp(s_result, "WIN") == 0)
		return(WIN);
	
	if (strcmp(s_result, "LOSS") == 0 || strcmp(s_result, "LOSE") == 0)
		return(LOSE);
	
	if (strcmp(s_result, "DRAW") == 0)
		return(DRAW);
	
	if (strcmp(s_result, "0") == 0		||
		strcmp(s_result, "0.0") == 0 	||
		strcmp(s_result, "1") == 0		||
		strcmp(s_result, "1.0") == 0	||
		strcmp(s_result, "0.5") == 0 )
		
		return(atof(s_result));
	
	return(-1);
}



/* Args:
 * 	c1	First elo rating
 * 	c2	Second elo rating
 * 	res	Winning perspective of first rating (c1) competitor
 * 	kf	K-factor (Optional, default 32)
 * 																*/
int main(int argc, char* argv[]){
	
	if (argc >= 4 && argc < 6) {
			
		uint32_t	c1, c2;
		float		res;
		
		c1 = (uint32_t)atoi(argv[1]);
		c2 = (uint32_t)atoi(argv[2]);
		
		res = parse_result(argv[3]);
		
		if (res == -1){
			errno = EINVAL;
			perror("Invalid result");
			exit(1);
		}
			
		if (argc == 5) g_kf = (int8_t)atoi(argv[4]);
			
		calc_set_elo(c1, c2, res);
		
	} else {
		
		errno = EINVAL;
		perror("Arguments");
		exit(1);
		
	}
	
	return(0);
}
