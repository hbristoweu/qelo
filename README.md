# Quick Elo #

QElo is a quick implementation of the Elo competitor ranking system in C.

## Arguments ##

Qelo requires 3 arguments are provided when called, with an optional 4th.

1. Elo rating of first competitor
2. Elo rating of second competitor
3. Match result perspective of first competitor (See Matchup Result Definitions) i.e. Did competitor 1 win, draw or lose.
4. (Optional) Custom K-Factor (Default 32)

There is an optional 4th parameter to specify a custom K-Factor. The default will be 32.


## Example execution, input and output ##


```
#!bash

$ ./qelo 2040 2090 DRAW
0.5     2024    2074
```

```
#!bash

$ ./qelo 2040 2090 0.5 64
0.5     2008    2058
```


## Matchup Result Definitions ##

The third parameter provides the result of the competitor matchup. The result can be one of three semantic values: Win, Loss or Draw

These values can be provided numerically or in a short uppercase string. For example:

* 1 is a win. 0 is a loss. 0.5 is a draw.
* **WIN** can be provided to specify a win instead of the numeric value 1.
* In the same vain, **DRAW**, **LOSS** and **LOSE** can be used in place of their respective numeric values.